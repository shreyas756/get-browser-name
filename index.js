function whichBrowser() {
  const userAgentString = navigator.userAgent;
  let chromeAgent = userAgentString.indexOf("Chrome") > -1;

  const IExplorerAgent =
    userAgentString.indexOf("MSIE") > -1 || userAgentString.indexOf("rv:") > -1;

  const firefoxAgent = userAgentString.indexOf("Firefox") === 70;

  let safariAgent = userAgentString.indexOf("Safari") > -1;

  if (chromeAgent && safariAgent) safariAgent = false;

  const operaAgent = userAgentString.indexOf("OP") > -1;

  if (chromeAgent && operaAgent) chromeAgent = false;

  if (chromeAgent) return "chrome";
  if (firefoxAgent) return "firefox";
  if (IExplorerAgent) return "IExplorer";
  if (safariAgent) return "safari";
  return "";
}

module.exports = whichBrowser;
